package controller;

import java.io.Serializable;

public class Human extends Player implements Serializable {
	
	// VARIABLES
	private int nbLife = 5;

	
	// CONSTRUTEUR
	public Human(String name) {
		super(name);
	}
	
	
	// ACCESSEURS (GETTEURS & SETTEURS)
	public int getNbLife() { return nbLife; }
	
}
