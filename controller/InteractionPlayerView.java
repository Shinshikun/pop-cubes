package controller;

import java.util.Scanner;

import launcher.*;
import objects.Level;

public class InteractionPlayerView {
	
	// VARIABLES
	protected Model model;
	protected Player player;
	private View mainView = null;
	private ViewTerminal viewTerminal = null;
	private Scanner scanAnswer = new Scanner(System.in);
	
	
	// CONSTRUCTEUR
	public InteractionPlayerView(Model m, View v, Player p) {
			this.model = m;
			this.mainView = v;
			this.player = p;
	}
	
	public InteractionPlayerView(Model m, ViewTerminal v, Player p) {
		this.model = m;
		this.viewTerminal = v;
		this.player = p;
	}

	// ACCESSEURS (GETTEURS & SETTEURS)
	public Scanner getScanAnswer() { return scanAnswer; }
	public Player getPlayer() { return player; }
	
	
	// METHODES POUR CHANGER DE PAGE SUR L'INTERFACE GRAPHIQUE
	
	public void changePage(String page) {
		this.mainView.changePage(page);
	}
		
	// METHODES POUR CHANGER DE PAGE SUR LE TERMINAL
	public void changeViewTerminal(String previousPage) {
		String res = "";
		switch(previousPage) {
			case "home" :
				res = makeAnswerValid("p", "o", "Que voulez-vous faire, voir le plateau (p) ou voir les options (o) ?");
				if(res.equals("p")) this.viewTerminal.viewBoardGame();
				else if(res.equals("o")) this.viewTerminal.viewOptions("");
				break;
			case "boardGame" :
				res = makeAnswerValid("j", "m", "Que voulez-vous faire, jouer à un niveau (j) ou revenir sur le menu (m) ?");
				if(res.equals("j")) {
					int l = chooseTheLevel();
					this.viewTerminal.viewFirstPageLevel(l);
				}
				else if(res.equals("m")) this.viewTerminal.displaysOnTheTerminal();
				break;
			default : break;
		}
	}
	
	public void changeViewTerminalForLevelPage(String previousPage, Level level) {
		String res = "";
		switch(previousPage) {
			case "first page level" :
				res = makeAnswerValid("j", "p", "Que voulez-vous faire, jouer au niveau (j) ou retourner au plateau (p) ?");
				if(res.equals("j")) {				
					try {
						Level copy = model.clone(level.getLvl());
						model.setPartieEnCours(copy);
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					}
					this.viewTerminal.viewSecondPageLevel();
				}
				else if(res.equals("p")) this.viewTerminal.viewBoardGame();
				break;
			case "level-run question" :
				System.out.println("Que voulez-vous faire, voir la légende des lettres (l), enlever des blocs (e), utiliser un bonus (b) ou abandonner (a) ?");
				res = scanAnswer.nextLine();
				while (!res.equals("l") && !res.equals("e") && !res.equals("b") && !res.equals("a")) {
					System.out.println("Que voulez-vous faire, voir la légende des lettres (l), enlever des blocs (e), utiliser un bonus (b) ou abandonner (a) ?");
					res = scanAnswer.nextLine();
				}
				if(res.equals("l")) this.viewTerminal.seeLegend();
				else if(res.equals("e")) {
					int[] coord = chooseTheCoordinates();
					model.getPartieEnCours().removeBloc(coord[0], coord[1]);
					model.getPartieEnCours().fall();
					model.getPartieEnCours().left();
					this.viewTerminal.viewSecondPageLevel();
				}
				else if(res.equals("b")) System.out.println("en cours de dev");
				else {
					model.setPartieEnCours(null);
					model.getIdAndColor().clear();
					this.viewTerminal.viewBoardGame();
				}
				break;
			default : break;
		}
	}
	
	private String makeAnswerValid(String a1, String a2, String question) {
		System.out.println(question);
		String res = "";
		res = scanAnswer.nextLine();
		while (!res.equals(a1) && !res.equals(a2)) {
			System.out.println(question);
			res = scanAnswer.nextLine();
		}
		return res;
	}
	
	private int chooseTheLevel() {
		System.out.println("Quel niveau voulez-vous faire ?");
		String res = scanAnswer.nextLine();
		Level l = null;
		if(Integer.parseInt(res) <= model.getBoardGame().getTotalNbLvls() && Integer.parseInt(res) > 0) {
			if(this.model.getBoardGame().getLevelTab()[Integer.parseInt(res)].isAvailable()) {
				l = this.model.getBoardGame().getLevelTab()[Integer.parseInt(res)];
			}
		}
		
		while (l == null) {
			System.out.println("Niveau indisponible !!");
			System.out.println("Quel niveau voulez-vous faire ?");
			res = scanAnswer.nextLine();
			if(Integer.parseInt(res) <= model.getBoardGame().getTotalNbLvls() && Integer.parseInt(res) > 0) {
				if(this.model.getBoardGame().getLevelTab()[Integer.parseInt(res)].isAvailable()) {
					l = this.model.getBoardGame().getLevelTab()[Integer.parseInt(res)];
				}
			}
		}
		return l.getLvl();
	}
	
	private int[] chooseTheCoordinates() {
		int[] coord = new int[2];
		int i = 0;
		int y = 0;
		String res = "";
		System.out.print("Indice de l'ordonée du bloc que vous voulez enlever : ");
		res = scanAnswer.nextLine();
		i = Integer.parseInt(res)-1;
		System.out.print("Indice de l'abcisse du bloc que vous voulez enlever : ");
		res = scanAnswer.nextLine();
		y = Integer.parseInt(res)-1;
		
		while(!model.getPartieEnCours().isInsideTheTabVisible(i, y)) {
			System.out.print("Indice de l'ordonée du bloc que vous voulez enlever : ");
			res = scanAnswer.nextLine();
			i = Integer.parseInt(res)-1;
			System.out.print("Indice de l'abcisse du bloc que vous voulez enlever : ");
			res = scanAnswer.nextLine();
			y = Integer.parseInt(res)-1;
		}
		
		coord[0] = i;
		coord[1] = y;
		return coord;
	}
}
