package controller;

import java.io.Serializable;

public abstract class Player implements Serializable {
	
	// VARIABLES
	private final String name;
	private int[] points = new int[11]; // PHRASE QUI RISQUE DE CHANGER
	private int nbPiece = 0;
	
	// CONSTRUCTEUR
	public Player(String name) {
		this.name = name;
	}
	
	// ACCESSEURS (GETTEURS & SETTEURS)
	public String getName() { return name; }
	public int getNbPiece() { return nbPiece; }
}
