package launcher;

import java.io.IOException;

import controller.Player;
import dataBase.SerializeObject;

/**
 * Cette classe est le main, elle sert a lancer le jeu.
 */

public class Launcher {
	
	// METHODE DE LANCEMENT
	/**
	 *  main 
	 *  @param String [] args
	 */
	public static void main (String[] args) throws ClassNotFoundException, IOException {
		SerializeObject so = new SerializeObject();
		so.serializeLevels(); // PHRASE QUI RISQUE DE CHANGER
		
		if(args.length == 1) {
			// FORMAT DES ARGUMENTS : --typeOfView=[...]
			String[] config = makeConfigArgs(args);
			
			if(config[0].equals("gui")) {
				View window = new View();
				window.setSize(600, 830);
				window.setVisible(true);
			} else {
				new ViewTerminal();
			}
		} else {
			displayHelpAndExit();
		}
	}
	
	/**
	 * Récupere les arguments de la commande
	 * @param args tableau qui contient les argument de la commande
	 * @return String []
	 */	
	static String[] makeConfigArgs(String[] args) {
		String[] res = new String[1];
		if (args[0].startsWith("--")) {
			String[] parts = args[0].split("=");
			if (parts.length == 2) {
				String pName = parts[0];
                String pValue = parts[1];
                if(pName.equals("--typeOfView")) {
                	if(pValue.equals("GUI")) res[0] = "gui";
                    else if(pValue.contentEquals("terminal")) res[0] = "terminal";
                    else displayHelpAndExit();
                } else {
                	displayHelpAndExit();
                }
			} else {
				displayHelpAndExit();
			}
		} else {
			displayHelpAndExit();
		}
		return res;
	}
	
	/**
	 * affiche un message d'erreur 
	 */	
	static void displayHelpAndExit() {
		System.out.println("Bonjour. Si vous arrivez à voir ce message, vous avez sans doute saisi une commande incorrecte.\n");
		System.out.println("Il y a deux possibilités pour jouer à notre jeu :\n");
		System.out.println("- Si vous voulez jouer à notre jeu sur l'interface graphique, alors écrivez cette commande :");
		System.out.println("java launcher.Launcher --typeOfView=GUI");
		System.out.println("Si vous voulez jouer sur le terminal, alors écrivez cette commande :");
		System.out.println("java launcher.Launcher --typeOfView=terminal\n\n");
	}
}

