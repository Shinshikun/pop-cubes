package launcher;

import java.util.Map;
import java.util.Scanner;

import controller.*;
import objects.Level;
import objects.Viewable;

/**
 * Cette classe est la vue du terminal
 */
public class ViewTerminal implements Viewable {
	
	// VARIABLES
	private Model model;
	private InteractionPlayerView interactionPlayerView;
	
	//CONSTRUCTEUR
	/**
	 * * Construis un objet de type ViewTerminal en lui donnant un joueur et un boolean
	 */
	public ViewTerminal() throws ClassNotFoundException {
		this.model = new Model(this);
		System.out.println("Bonjour. Vous êtes un Humain (h) ou un Robot (r) ?");
		String res = "";
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		res = sc.nextLine();
		while (!res.equals("h") && !res.equals("r")) {
			System.out.println("Vous êtes un Humain (h) ou un Robot (r) ?");
			res = sc.nextLine();
		}
		System.out.println("Quel est ton nom ?");
		String res2 = sc.nextLine();
		Player p = (res.equals("h") ? new Human(res2) : new Robot(res2));
		interactionPlayerView = new InteractionPlayerView(model, this, p);
		model.serializePlayer(p);
		
		
		displaysOnTheTerminal();
	}
	
	// METHODES
	@Override
	public void displaysOnTheTerminal() {
		System.out.println("\n");
		System.out.println("##############################");
		System.out.println("#                            #");
		System.out.println("#       Bienvenue sur        #");
		System.out.println("#  l'interface du terminal   #");
		System.out.println("#     du jeu Pop Cubes !     #");
		System.out.println("#                            #");
		System.out.println("##############################");
		System.out.println();
		interactionPlayerView.changeViewTerminal("home");
	}
	
	/**
	 *Affiche le plateau de jeu
	 */
	public void viewBoardGame() {
		model.getBoardGame().displaysOnTheTerminal();
		interactionPlayerView.changeViewTerminal("boardGame");
	}
	
	/**
	 *Affiche les option
	 *@param String page
	 */	
	public void viewOptions(String page) {
		if(page.equals("")) {
			System.out.println("\n");
			System.out.println("Vous pouvez voir les crédits (c), les règles du jeu (r), les booters (b) ou revenir au menu (m).");
			System.out.println("Que voulez-vous faire ?");
			String res = "";
			res = interactionPlayerView.getScanAnswer().nextLine();
			while (!res.equals("c") && !res.equals("r") && !res.equals("b") && !res.equals("m")) {
				System.out.println("Vous pouvez voir les crédits (c), les règles du jeu (r), les booters (b) ou revenir au menu (m).");
				System.out.println("Que voulez-vous faire ?");
				res = interactionPlayerView.getScanAnswer().nextLine();
			}
			viewOptions(res);
			return;
		} else {
			if(page.equals("c")) {
				System.out.println("Tara Aggoun et Elody Tang");
				System.out.println("Vous allez être redirigé vers la page des options.");
				viewOptions("");
			} else if(page.equals("r")) {
				System.out.println("Appuie sur les groupes de blocs pour les éliminer.");
				System.out.println("Sauve les animaux en les amenant en bas de l'écran.");
				System.out.println("Vous allez être redirigé vers la page des options.");
				viewOptions("");
			} else if(page.equals("b")) {
				// TODO REMPLIR LES BONS BOOSTERS
				System.out.println("Boule Disco : enlève tous les blocs de la même couleur aléatoirement (D)");
				System.out.println("Fusée : enlève les blocs d'une ligne entière ou d'une colonne (F)");
				System.out.println("Marteau : enlève le bloc sélectionne (M)");
				System.out.println("Bombe : enlève les blocs adjacentes du bloc sélectionné (B)");
				System.out.println("Vous allez être redirigé vers la page des options.");
				viewOptions("");
			} else {
				displaysOnTheTerminal();
			}
		}
	}
	
	/**
	 *Affiche la premiere page des niveaux
	 *@param int nbLevel
	 */	
	public void viewFirstPageLevel(int nbLevel) {
		Level level = model.getBoardGame().getLevelTab()[nbLevel];
		System.out.println("\n");
		System.out.println("Objectifs à atteindre : ");
		System.out.println("(1) Sauve " + level.getGoals().getAnimalToSave() + " animaux.");
		System.out.println("(2) Obtiens " + level.getGoals().getPoints() + " points.\n");
		System.out.println("Les joueurs peuvent commencer avec des booters (Bombe (B), Fusee (F), Boule Disco (B), Marteau (M)) en début de partie. Pour mettre en avant l'interface graphique, les booters ne sont pas disponibles sur cette interface.\n");
		interactionPlayerView.changeViewTerminalForLevelPage("first page level", level);
	}
	
	/**
	 *Affiche la seconde page des niveaux
	 */	
	public void viewSecondPageLevel() {
		System.out.print("\n");
		if(model.getIdAndColor().size() == 0) {
			model.addIdColor();
		}
		Map<Integer, String> idAndColor = model.getIdAndColor();
		model.getPartieEnCours().displaysOnTheTerminal(idAndColor);
		interactionPlayerView.changeViewTerminalForLevelPage("level-run question", model.getPartieEnCours());
	}
	
	/**
	 *Affiche les legendes
	 */	
	public void seeLegend() {
		System.out.println("\n");
		System.out.println("Légende des lettres :");
		System.out.println("A : Animal");
		System.out.println("# : Mur");
		System.out.println("R : Bloc Rouge (red)");
		System.out.println("Y : Bloc Jaune (yellow)");
		System.out.println("P : Bloc Violet (purple)");
		System.out.println("B : Bloc Bleu (blue)");
		System.out.println("G : Bloc Vert (green)");
		
		// TODO : Ajouter les bonus
		
		System.out.println();
		interactionPlayerView.changeViewTerminalForLevelPage("level-run question", model.getPartieEnCours());
	}
}
