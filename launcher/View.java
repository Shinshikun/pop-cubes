package launcher;

import java.awt.*;
import java.util.Map;

import javax.swing.*;

import controller.*;
import controller.Robot;
import objects.*;

/**
 * Cette classe est la vue de l'interface graphique
 */
public class View extends JFrame {
	
	// VARIABLES
	private Model model;
	private InteractionPlayerView interactionPlayerView;
	private CardLayout cardLayout = new CardLayout();
	private JPanel mainPanelBold = new JPanel(cardLayout);
	
	// CONSTRUCTEUR
	/**
	 * Construis un objet de type View en lui donnant un joueur et un boolean
	 */
	public View() throws ClassNotFoundException {
		UIManager.put("ProgressBar.selectionBackground", Color.black);
        UIManager.put("ProgressBar.selectionForeground", Color.white);
        UIManager.put("ProgressBar.foreground", new Color(81,123,113,200));
		this.model = new Model(this);
		
		this.setTitle("Pop Cubes");
		this.setResizable(false);
		this.setAlwaysOnTop(true);
		
		this.getContentPane().add(mainPanelBold);
		
		this.mainPanelBold.add("param", paramAndPlayer());
		cardLayout.show(mainPanelBold, "param");
		
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	// METHODES & FONCTIONS
	
	// METHODE POUR AJOUTER DES ELEMENTS AU PANEL PRINCIPAL
	/**
	 * Ajoute un element
	 */
	public void addElement() {
		JPanel home = toTheHomePage();
		mainPanelBold.add("home", home);
		
		JPanel homeParam = paramAndPlayer();
		mainPanelBold.add("param", homeParam);
		
		JPanel options = toTheOptionsPage();
		mainPanelBold.add("options", options);
		
		String[] textRules = {"Règles du jeu", "Objectifs", "<html><p align=\"center\">Sauve les animaux en les amenant en bas de l'écran.</p></html>", "<html><p align=\"center\">Clique sur les groupes de blocs pour les éliminer !</p></html>"};
		JPanel rules = toSimplePage("/medias/view/rules-page.png", textRules, "options", true);
		mainPanelBold.add("rules", rules);
		
		String[] textCredits = {"Crédits", "Production", "<html><p align=\"center\">Tara Aggoun<br> taraggoun@gmail.com</p></html>", "<html><p align=\"center\">Elody Tang<br>elodytang@hotmail.fr</p></html>"};
		JPanel credits = toSimplePage("/medias/view/credits-page.png", textCredits, "options", true);
		mainPanelBold.add("credits", credits);
		
		String[] textHelp = {"Explications des boosters", "Fusée", "Enlève tous les blocs sur une même colonne.", "Boule de Disco", "Enlève tous les blocs de la même couleur (choisie).", "Bombe", "Enlève tous les blocs adjacents d'un bloc.", "Marteau", "Enlève uniquement un bloc."};
		JPanel help = toSimplePage("/medias/view/help-page.png", textHelp, "options", false);
		mainPanelBold.add("help", help);
		
		JPanel boardGame = toTheBoardGame();
		mainPanelBold.add("boardGame", boardGame);
		
		String[] textMaj = {"Mise à jour !", "PLUS DE NIVEAUX ?", "<html><p align=\"center\">De nouveaux niveaux <br>arrivent bientôt !</p></html>", "<html><p align=\"center\">Ce jeu est en cours de développement. Restez à l'écoute !</p></html>"};
		JPanel maj = toSimplePage("/medias/view/maJ-page.png", textMaj, "boardGame", true);
		mainPanelBold.add("maj", maj);
	}
	
	/**
	 * Ajoute un niveau
	 * @Level level
	 */
	public void addLevels(Level level) {
		JPanel firstPageL = toFirstPageOfLevel(level);
		mainPanelBold.add("firstPageL" + level.getLvl(), firstPageL);
	}
	
	/**
	 * Ajoute la page du niveau
	 */
	public void addPageLevels() {
		JPanel secondPageL = toLevelPage();
		mainPanelBold.add("secondPageL" + model.getPartieEnCours().getLvl(), secondPageL);
	}
	
	// METHODES POUR CHANGER DE VUE
	/**
	 * Change de page
	 * @param String page
	 */
	public void changePage(String page) {
		cardLayout.show(mainPanelBold, page);
	}
	
	// METHODES POUR FACILITER LA CREATION DES VUES
	
	/**
	 * Creation d'un beau bouton avec une Icon en fond et un texte blanc
	 * @param String text (texte qui apparait sur le bouton)
	 * @param String pathIcon (chemin de l'image en fond)
	 * @return JButton res
	 */
	public JButton makeABeautifulButton(String text, String pathIcon) {
		JButton res = new JButton(text, new ImageIcon(getClass().getResource(pathIcon)));
		res.setBackground(new Color(0,0,0,0));
		res.setFont(new Font("SansSerif", Font.PLAIN, 20));
		res.setForeground(new Color(86,86,86));
		res.setBorderPainted(false);
		res.setVerticalTextPosition(SwingConstants.CENTER);
		res.setHorizontalTextPosition(SwingConstants.CENTER);
		return res;
	}
	
	/**
	 * Creation d'un JLabel qui sera le fond d'écran de la vue 
	 * @param String pathIcon (c'est le chemin de l'image en fond)
	 * @param JPanel p panneau
	 */
	public void makeABeautifulBackground(String pathIcon, JPanel p) {
		JLabel res = makeABeautifulLabel(pathIcon);
		res.setBounds(0, 0, 600, 800);
		p.add(res);
	}
	
	/**
	 * Creation d'un JLabel qui sera le fond d'écran de la vue 
	 * @param String pathIcon (c'est le chemin de l'image en fond)
	 * @return JLabel res
	 */
	public JLabel makeABeautifulLabel(String pathIcon) {
		JLabel res = new JLabel();
		res.setIcon(new ImageIcon(getClass().getResource(pathIcon)));
		res.setHorizontalAlignment(0);
		return res;
	}
	
	/**
	 * Creation d'un bouton X 
	 * @param String out (page de l'event quand on clique dessus)
	 * @return JButton X
	 */
	public JButton makeAXButton(String out) {
		JButton X = makeABeautifulButton(null, "/medias/view/X.png");
		X.setBounds(20, 530, 84, 60);
		X.addActionListener ((event) -> {
			if(!out.equals("")) {
				interactionPlayerView.changePage(out);
			} else {
				this.dispose();
			}
		});
		return X;
	}
	
	/**
	 * Creation d'un JPanel qui sera le fond d'un rectangle de la vue 
	 * @param Color c 
	 * @return JPanel mainPanel
	 */
	public JPanel makeTheRectangleWhite(Color c, String title, String out) {
		JPanel mainPanel = new JPanel(null);
		mainPanel.setBackground(Color.WHITE);
		mainPanel.setBounds(50, 100, 500, 600);
		mainPanel.setBorder(BorderFactory.createLineBorder(c, 5));
		
		JButton X = makeAXButton(out);
		mainPanel.add(X);
		
		JLabel t = new JLabel(title, 0);
		t.setFont(new Font("SansSerif", Font.BOLD, 25));
		t.setForeground(Color.white);
		t.setOpaque(true);
		t.setBackground(new Color(88,188,169));
		t.setBounds(0, 0, 500, 80);
		mainPanel.add(t);
		
		JLabel end = new JLabel();
		end.setOpaque(true);
		end.setBackground(new Color(88,188,169));
		end.setBounds(0, 520, 500, 80);
		mainPanel.add(end);
		
		return mainPanel;
	}
	
	/**
	 * Creation d'un Radiobutton
	 * @param String name
	 * @param ButtonGroup b
	 * @return JRadioButton res
	 */
	public JRadioButton makeBeautifulRadioButton(String name, ButtonGroup b) {
		JRadioButton res = new JRadioButton(name);
		res.setFont(new Font("SansSerif", Font.PLAIN, 15));
		res.setHorizontalAlignment(0);
		res.setOpaque(false);
		b.add(res);
		return res;
	}
	
	/**
	 * Creation d'un JPanel qui sera la grille de boosters
	 * // Si b est true, alors les evenements sont pour le panel 'first page level'
	 * // Si c'est false, alors c'est pour celui qui est dans 'second page level'
	 * @param Color c
	 * @param b
	 * @return JPanel booster
	 */
	public JPanel makeGridBoosters(Color c, boolean page) {
		GridLayout grid = new GridLayout(1,4);
		grid.setHgap(10);
		JPanel booster = new JPanel(grid);
		booster.setBackground(c);
		String[] boosters = {"bomb", "rocket", "disco-ball", "hammer"};
		if(page) {
			for(int i=0; i<boosters.length; i++) {
				boosters[i] = boosters[i] + "_2";
			}
		}
		
		for(int i=0; i<boosters.length; i++) {
			int x = i;
			JButton b = makeABeautifulButton(null, "/medias/objects/boosters/" + boosters[i] + ".png");
			b.setBorderPainted(true);
			if(!page) {
				b.setBackground(new Color(250, 248, 225));
				b.setBorderPainted(false);
				b.setEnabled(false);
				
				//TODO : EVENT
			} else {
//				b.setEnabled(false);
				b.setBorder(BorderFactory.createLineBorder(Color.white, 1));
				b.addActionListener((event) -> {
					String[] parts = boosters[x].split("_");
					model.events.put(parts[0], true);
					System.out.println(parts[0] + " sélectionnée");
				});
			}
				
			b.setBorder(BorderFactory.createLineBorder(Color.white, 1));
			booster.add(b);
		}
		
		return booster;
	}
	
	/**
	 * Creation d'une page simple, avec un JPanel au milieu et une image 
	 * @param String pathIcon (c'est le chemin de l'image en fond)
	 * @param String title (titre de la page)
	 * @param String out (nom de la page de sortie)
	 */
	public JPanel toSimplePage(String pathImage, String[] text, String out, boolean page) {
		JPanel mainPanel = new JPanel(null);
		
		// Creation du panneau principal
		JPanel rectangle = makeTheRectangleWhite(Color.white, text[0], out);
		mainPanel.add(rectangle);
		JPanel content = simplePageParam(pathImage, text, page);
		rectangle.add(content);

		// Creation du background
		this.makeABeautifulBackground("/medias/view/backgroundSimple2.png", mainPanel);
		return mainPanel;
	}
	
	private JPanel simplePageParam(String pathImage, String[] text, boolean page) {
		JPanel content = new JPanel(null);
		if(page) {
			GridLayout grid = new GridLayout(1,2);
			grid.setHgap(20);
			content.setLayout(grid);
			simplePageContentTrue(content, text);
			JLabel image = makeABeautifulLabel(pathImage);
			content.add(image);		
		} else {
			GridLayout grid = new GridLayout(4,1);
			grid.setVgap(20);
			content.setLayout(grid);
			
			for(int i=-1; i<=5; i += 2) {
				String[] res = new String[3];
				res[0] = text[i+2];
				res[1] = text[i+1+2];
				
				if(res[0].equals("Fusée")) res[2] = "rocket";
				else if(res[0].equals("Boule de Disco")) res[2] = "disco-ball";
				else if(res[0].equals("Bombe")) res[2] = "bomb";
				else res[2] = "hammer";
				
				simplePageContentFalse(content, res);
			}
		}
		content.setBounds(50, 150, 400, 300);
		content.setBackground(Color.white);
		
		return content;
	}
	
	private void simplePageContentTrue(JPanel content, String[] text) {
		JPanel groupText = new JPanel(null);
		groupText.setBackground(Color.white);
		groupText.setBounds(0, 0, 200, 300);
		content.add(groupText);
		
		JLabel t1 = new JLabel(text[1], 0);
		t1.setBounds(0, 0, 200, 30);
		t1.setOpaque(true);
		t1.setFont(new Font("SansSerif", Font.PLAIN, 15));
		t1.setBackground(new Color(238,229,203));
		groupText.add(t1);
		
		GridLayout grid1 = new GridLayout(2,1);
		grid1.setVgap(10);
		JPanel textG = new JPanel(grid1);
		textG.setBackground(Color.white);
		textG.setBounds(0, 50, 200, 260);
		groupText.add(textG);
		
		String[] txtString = {text[2], text[3]};
		for(int i=0; i<txtString.length; i++) {
			JLabel txt = new JLabel(txtString[i], 0);
			txt.setFont(new Font("SansSerif", Font.PLAIN, 13));
			txt.setOpaque(true);
			txt.setBackground(new Color(250, 248, 225));
			textG.add(txt);
		}
	}
	
	private void simplePageContentFalse(JPanel content, String[] parts) {
		JPanel groupText = new JPanel(null);
		groupText.setBackground(Color.white);
		content.add(groupText);
		
		JLabel image = makeABeautifulLabel("/medias/objects/boosters/" + parts[2] + ".png");
		image.setBounds(0, 0, 65, 65);
		groupText.add(image);
		
		JLabel t3 = new JLabel(parts[0], 0);
		t3.setBounds(80, 0, 100, 20);
		t3.setOpaque(true);
		t3.setFont(new Font("SansSerif", Font.PLAIN, 10));
		t3.setBackground(new Color(238,229,203));
		groupText.add(t3);
		
		JLabel t4 = new JLabel(parts[1], 0);
		t4.setBounds(80, 30, 320, 30);
		t4.setOpaque(true);
		t4.setFont(new Font("SansSerif", Font.PLAIN, 10));
		t4.setBackground(new Color(250, 248, 225));
		groupText.add(t4);
	}
	
	// METHODES POUR CREER DES JPANEL FACILEMENT

	/**
	 * Creation de la page d'accueil et son affichage
	 */
	public JPanel toTheHomePage() {
		JPanel mainPanel = new JPanel(null);
		
		JButton playNow = makeABeautifulButton("Jouer", "/medias/view/template-button-bold.png");
		playNow.setFont(new Font("SansSerif", Font.BOLD, 25));
		playNow.setForeground(Color.white);
		playNow.setBounds(140, 675, 320, 65);
		mainPanel.add(playNow);
		playNow.addActionListener ((event) -> {
			interactionPlayerView.changePage("boardGame");
		});
	
		JButton options = makeABeautifulButton("", "/medias/view/button-options.png");
		options.setBounds(10, 10, 40, 40);
		mainPanel.add(options);
		options.addActionListener ((event) -> {
			interactionPlayerView.changePage("options");
		});
		
		// Creation du background
		this.makeABeautifulBackground("/medias/view/background-with-logo.png", mainPanel);
		return mainPanel;
	}
	
	/**
	 * Creation d'un JPanel qui sera la page de menue
	 * @return JPanel mainPanel
	 */
	public JPanel paramAndPlayer() {
		JPanel mainPanel = new JPanel(null);
		
		// Creation du bouton JOUER
		JButton play = makeABeautifulButton("Valider", "/medias/view/template-button.png");
		play.setForeground(Color.white);
		play.setFont(new Font("SansSerif", Font.BOLD, 25));
		play.setBounds(210, 630, 180, 60);
		mainPanel.add(play);
		
		JPanel rectangle = this.makeTheRectangleWhite(Color.white, "Récupération des données", "");
		mainPanel.add(rectangle);
		
		// Creation d'un panneau pour connaître les paramètres
		JPanel content = new JPanel(null);
		content.setBounds(50, 130, 400, 350);
		content.setBackground(Color.white);
		rectangle.add(content);
		
		// Creation des boutons
		ButtonGroup typeOfPlayer = new ButtonGroup();
		JRadioButton human = makeBeautifulRadioButton("Humain", typeOfPlayer);
		JRadioButton robot = makeBeautifulRadioButton("Robot", typeOfPlayer);
		ButtonGroup newPlayer = new ButtonGroup();
		JRadioButton yes = makeBeautifulRadioButton("Oui", newPlayer);
		JRadioButton no = makeBeautifulRadioButton("Non", newPlayer);
		
		
		// Nouveau joueur ou pas ?
		pageParamSubTitle(content, "Nouveau joueur ?", 0);
		
		GridLayout grid = new GridLayout(1,2);
		grid.setHgap(30);
		JPanel yesNo = new JPanel(grid);
		yesNo.setBackground(new Color(250, 248, 225));
		yesNo.setBounds(0, 40, 400, 60);
		content.add(yesNo);
		
		yes.setSelected(true);
		yesNo.add(yes);
		yesNo.add(no);

		pageParamSubTitle(content, "Type de joueur ?", 130);
		
		JPanel humanRobot = new JPanel(grid);
		humanRobot.setBackground(new Color(250, 248, 225));
		humanRobot.setBounds(0, 170, 400, 60);
		content.add(humanRobot);
		human.setSelected(true);
		
		humanRobot.add(human);
		humanRobot.add(robot);
		
		pageParamSubTitle(content, "Nom ?", 260);
		
		JTextField nameField = new JTextField();
		nameField.setBounds(0, 300, 400, 30);
		nameField.setHorizontalAlignment(0);
		nameField.setBorder(BorderFactory.createLineBorder(new Color(250,248,225), 2));
		content.add(nameField);
		
		play.addActionListener((event) -> {
			if(!nameField.getText().equals("")) {
				Player p;
				if(yes.isSelected()) {
					p = (human.isSelected() ? new Human(nameField.getText()) : new Robot(nameField.getText()));
					model.serializePlayer(p);
					interactionPlayerView = new InteractionPlayerView(model, this, p);
					this.addElement();
					interactionPlayerView.changePage("home");
				} else {
					p = model.deserializablePlayer(nameField.getText());
					if(p != null) {
						interactionPlayerView = new InteractionPlayerView(model, this, p);
						this.addElement();
						interactionPlayerView.changePage("home");
					}
				}
			}
		});
		
		// Creation du background
		this.makeABeautifulBackground("/medias/view/backgroundSimple2.png", mainPanel);
		return mainPanel;
	}
	
	private void pageParamSubTitle(JPanel content, String title, int diff) {
		JLabel t = new JLabel(title, 0);
		t.setBounds(0, 0 + diff, 400, 30);
		t.setOpaque(true);
		t.setFont(new Font("SansSerif", Font.PLAIN, 15));
		t.setBackground(new Color(238,229,203));
		content.add(t);
	}
	
	/**
	 * Creation de la page des options
	 */
	public JPanel toTheOptionsPage() {
		JPanel mainPanel = new JPanel(null);
		
		JPanel rectangle = makeTheRectangleWhite(Color.white, "Options", "home");
		mainPanel.add(rectangle);
		
		GridLayout grid = new GridLayout(3,1);
		grid.setVgap(10);
		JPanel content = new JPanel(grid);
		content.setBounds(50, 130, 400, 350);
		content.setBackground(Color.white);
		rectangle.add(content);
		
		JButton[] button = makeButtonsForTheOptionsPage();
		for(int i=0; i<button.length; i++) {
			content.add(button[i]);
		}
		
		// Creation du background
		this.makeABeautifulBackground("/medias/view/backgroundSimple2.png", mainPanel);
		return mainPanel;
	}
	
	/**
	 * Creation d'un JLabel pour la page des option 
	 * @return JButton button
	 */
	public JButton[] makeButtonsForTheOptionsPage() {
		JButton[] button = new JButton[3];
		String[] title = {"Règles du jeu", "Explication des boosters", "Crédits"};
		
		for(int i=0; i<3; i++) {
			int x = i;
			button[i] = makeABeautifulButton(title[i], "/medias/view/template-button-simple.png");
			button[i].addActionListener((event) -> {
				String out = "";
				if(title[x].equals("Règles du jeu")) out = "rules";
				else if(title[x].equals("Explication des boosters")) out = "help";
				else out = "credits";
				interactionPlayerView.changePage(out);
			});
		}
		return button;
	}
	
	/**
	 * Creation de la page du plateau
	 */
	public JPanel toTheBoardGame() {
		JPanel mainPanel = new JPanel(null);
		
		// Affichage du JPanel pour le titre
		makeButtonUpPanel(mainPanel);
		
		// Creation du JPanel principal pour les boutons
		int lvlMin = (model.getNbPages() == 1 ? 1 : model.getNbPages() * model.getNB_LVL_IN_A_PAGE() - model.getNB_LVL_IN_A_PAGE() - 1);
		makeGridLevels(mainPanel, lvlMin);		
		
		// Affichage des boutons de changement de page
		JButton[] button = makeButtonForTheBoardGamePage(lvlMin);
		mainPanel.add(button[0]);
		if(model.getNbPages() != 1) mainPanel.add(button[1]);
		
		this.makeABeautifulBackground("/medias/view/backgroundSimple2.png", mainPanel);
		return mainPanel;
	}
	
	private void makeButtonUpPanel(JPanel up) {
		Player player = interactionPlayerView.getPlayer();
		
		JPanel content = new JPanel(null);
		content.setBounds(10, 20, 590, 45);
		content.setBackground(new Color(0,0,0,0));
		up.add(content);
		
		// Vie
		JLabel lifeIcon = this.makeABeautifulLabel("/medias/view/life.png");
		lifeIcon.setBounds(0, 0, 40, 40);
		content.add(lifeIcon);
		
		JLabel life = new JLabel((player instanceof Human ? Integer.toString(((Human) player).getNbLife()) : "Illimité") + "   ", SwingConstants.RIGHT);
		life.setBackground(Color.white);
		life.setFont(new Font("SansSerif", Font.BOLD, 12));
		life.setOpaque(true);
		life.setBounds(10, 10, 120, 25);
		content.add(life);
		
		// Piece
		JLabel diamondIcon = this.makeABeautifulLabel("/medias/view/diamond.png");
		diamondIcon.setBounds(140, 0, 40, 40);
		content.add(diamondIcon);
		
		JLabel piece = new JLabel(Integer.toString(player.getNbPiece()) + "   ", SwingConstants.RIGHT);
		piece.setBackground(Color.white);
		piece.setFont(new Font("SansSerif", Font.BOLD, 12));
		piece.setOpaque(true);
		piece.setBounds(150, 10, 120, 25);
		content.add(piece);
		
		// Retour Param
		JButton options = makeABeautifulButton("", "/medias/view/button-options.png");
		options.setBounds(520, 0, 40, 40);
		content.add(options);
		options.addActionListener ((event) -> {
			interactionPlayerView.changePage("options");
		});
	}
	
	private void makeGridLevels(JPanel center, int lvlMin) {
		
		// Rajout de la bannière
		JPanel banner = new JPanel();
		banner.setBounds(140, 100, 320, 61);
		banner.setBackground(new Color(0,0,0,0));
			
		JLabel titleLabel = new JLabel("Monde " + model.getNbPages(), new ImageIcon(getClass().getResource("/medias/view/banner-label.png")), 0);
		titleLabel.setForeground(Color.white);
		titleLabel.setFont(new Font("SansSerif", Font.BOLD, 20));
		titleLabel.setVerticalTextPosition(SwingConstants.CENTER);
		titleLabel.setHorizontalTextPosition(SwingConstants.CENTER);
		
		banner.add(titleLabel);
		center.add(banner);
		
		// Rajout des levels
		GridLayout grid = new GridLayout(5,4);
		grid.setHgap(40);
		grid.setVgap(40);
		JPanel panelBut = new JPanel(grid);
		panelBut.setBackground(new Color(0,0,0,0));
		panelBut.setBounds(100, 200, 400, 400);
		center.add(panelBut);
		
		// Retrouver le dernier niveau affiché sur la page
		int lvlMax = lvlMin + model.getNB_LVL_IN_A_PAGE();
		
		for(int i=lvlMin; i<lvlMax; i++) {
			JButton n = new JButton(Integer.toString(i));
			n.setBackground(new Color(0,0,0,0));
			n.setBorder(BorderFactory.createLineBorder(new Color(247,101,99), 2));
			n.setFont(new Font("SansSerif", Font.TRUETYPE_FONT, 15));
			n.setForeground(new Color(81, 123, 113));
			n.setEnabled(false);
			if(i < model.getBoardGame().getLevelTab().length) {
				Level level = model.getBoardGame().getLevelTab()[i];
				if(level.isAvailable()) {
					n.setEnabled(true);
					n.setBackground(Color.white);
				}
				n.addActionListener((event) -> {
					this.addLevels(level);
					interactionPlayerView.changePage("firstPageL" + level.getLvl());
				});
			}
			panelBut.add(n);
		}
	}
	
	/**
	 * Creation du boutton pour la page du plateau
	 * @param int niveauMin
	 */
	public JButton[] makeButtonForTheBoardGamePage(int niveauMin) {
		JButton[] button = new JButton[2];
		for(int i=0; i<=1; i++) {
			if(i == 0) {
				button[i] = new JButton(">");
				button[i].setBounds(550, 375, 50, 50);
				button[i].addActionListener((event) -> {
					if(model.getNbPages() != model.getTotalNbPages()) {
						model.setNbPages(model.getNbPages() + 1);
						interactionPlayerView.changePage("boardGame");
					} else {
						interactionPlayerView.changePage("maj");
					}
				});
			} else {
				button[i] = new JButton("<");
				button[i].setBounds(0, 375, 50, 50);
				button[i].addActionListener((event) -> {
					model.setNbPages(model.getNbPages() - 1);
					interactionPlayerView.changePage("boardGame");
				});
			}
			button[i].setBackground(new Color(0,0,0,0));
			button[i].setBorderPainted(false);
			button[i].setFont(new Font("SansSerif", Font.BOLD, 10));
			button[i].setForeground(new Color(81, 123, 113));
		}
		return button;
	}
	
	/**
	 * Creation de la premiere page du niveau
	 * Lorsqu'on clique sur le bouton du niveau, cette page apparait.
	 * Le joueur peut choisir d'y jouer ou pas
	 * @param Level level ()
	 */
	public JPanel toFirstPageOfLevel(Level level) {
		JPanel mainPanel = new JPanel(null);
		
		// Creation du bouton JOUER
		JButton play = makeABeautifulButton("Jouer", "/medias/view/template-button.png");
		play.setForeground(Color.white);
		play.setFont(new Font("SansSerif", Font.BOLD, 25));
		play.setBounds(210, 630, 180, 60);
		mainPanel.add(play);
		play.addActionListener ((event) -> {
			try {
				Level copy = model.clone(level.getLvl());
				model.setPartieEnCours(copy);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			this.addPageLevels();
			interactionPlayerView.changePage("secondPageL" + model.getPartieEnCours().getLvl());
		});
		
		// Creation du panneau principal
		JPanel rectangle = makeTheRectangleWhite(Color.white, "Niveau " + level.getLvl(), "boardGame");
		mainPanel.add(rectangle);
		
		// Creation du panneau central
		JPanel panneauCentral = makeCenterPanelFirstPageLevel(level);
		rectangle.add(panneauCentral);
		
		// Creation du background
		this.makeABeautifulBackground("/medias/view/backgroundSimple2.png", mainPanel);
		return mainPanel;
	}
	
	public JPanel makeCenterPanelFirstPageLevel(Level level) {
		JPanel cPanel = new JPanel(new GridLayout(2,1));
		cPanel.setBounds(50, 130, 400, 350);
		cPanel.setBackground(Color.white);
		
		JPanel goals = makeGoals(level);
		cPanel.add(goals);
		
		// Creation de la grille des boosters
		JPanel panneauBooster = makeGridBoostersFirstPageLevel();	
		cPanel.add(panneauBooster);
	
		return cPanel;
	}

	/**
	 * Creation de la page des objectifs
	 * @param Level level
	 * @return goals
	 */
	public JPanel makeGoals(Level level) {
		JPanel goals = new JPanel(null);
		goals.setBackground(Color.white);
		
		JLabel t1 = new JLabel("Objectifs", 0);
		t1.setBounds(0, 0, 400, 30);
		t1.setOpaque(true);
		t1.setFont(new Font("SansSerif", Font.PLAIN, 20));
		t1.setBackground(new Color(238,229,203));
		goals.add(t1);
		
		GridLayout grid = new GridLayout(2,1);
		grid.setVgap(10);
		JPanel goalsPanel = new JPanel(grid);
		goalsPanel.setBackground(Color.white);
		goalsPanel.setBounds(0, 40, 400, 135);
		goals.add(goalsPanel);
		
		String[] txtObj = {"Sauve " + level.getGoals().getAnimalToSave() + " animaux.", "Obtiens " + level.getGoals().getPoints() + " points."};
		for(int i=0; i<txtObj.length; i++) {
			JLabel txt = new JLabel(txtObj[i], 0);
			txt.setFont(new Font("SansSerif", Font.PLAIN, 15));
			txt.setOpaque(true);
			txt.setBackground(new Color(250, 248, 225));
			goalsPanel.add(txt);
		}
		return goals;
	}
	
	private JPanel makeGridBoostersFirstPageLevel() {
		JPanel boosters = new JPanel(null);
		boosters.setBackground(Color.white);
		
		JLabel t1 = new JLabel("Boosters", 0);
		t1.setBounds(0, 30, 400, 30);
		t1.setOpaque(true);
		t1.setFont(new Font("SansSerif", Font.PLAIN, 20));
		t1.setBackground(new Color(238,229,203));
		boosters.add(t1);
		
		JPanel bPanel = makeGridBoosters(Color.white, false);
		bPanel.setBounds(0, 70, 400, 100);
		boosters.add(bPanel);
		
		return boosters;
	}
	
	/**
	 * Création de la deuxième page de niveau
	 * Lorsqu'on clique sur "Jouer" de la première page, alors celle-ci apparaît.
	 * @return JPanel
	 */
	public JPanel toLevelPage() {
		JPanel mainPanel = new JPanel(null);
		
		// Creation du panneau du haut
		JPanel upPanel = makeGridUpLevel(model.getPartieEnCours());
		mainPanel.add(upPanel);
		
		// Creation du JPanel principal
		JPanel game = gridLevel(model.getPartieEnCours());
		mainPanel.add(game);
		
		// Creation du grid des boosters et la page de sortie
		JPanel booster = makeGridBoosters(new Color(0,0,0,0), true);
		booster.setBounds(200, 735, 200, 40);
		mainPanel.add(booster);
		
		JButton out = this.makeABeautifulButton("", "/medias/view/button-back.png");
		out.setBounds(25, 735, 56, 40);
		out.addActionListener((event) -> {
			model.setPartieEnCours(null);
			model.getIdAndColor().clear();
			interactionPlayerView.changePage("boardGame");
		});
		mainPanel.add(out);
		
		this.makeABeautifulBackground("/medias/view/backgroundSimple.png", mainPanel);
		
		return mainPanel;
	}
	
	/**
	 * Creation de la grille de niveau
	 * @param Level level
	 * @return JPanel game
	 */
	public JPanel gridLevel(Level level) {
		GamePiece[][] g = model.getPartieEnCours().getGamePiece();
		JPanel game = new JPanel(new GridLayout(6,7));
		game.setBorder(BorderFactory.createLineBorder(new Color(81,123,113), 5));
		game.setBounds(72, 170, 455, 390);
		game.setBackground(new Color(0,0,0,100));

		int min = (level.haveTheFirstLineNoNull() + 6 >= level.getGamePiece().length ? level.getGamePiece().length : level.haveTheFirstLineNoNull() + 6);
		for (int i=level.haveTheFirstLineNoNull(); i<min; i++) {
			for(int j=0; j<g[i].length; j++) {
				JButton buttonBlocAnimal = new JButton();
				if(g[i][j] != null) {
					if(g[i][j] instanceof Bloc) {
						Bloc b = (Bloc) g[i][j];
						if(model.getIdAndColor().size() == 0) {
							model.addIdColor();
						}
						Map<Integer, String> idAndColor = model.getIdAndColor();
						buttonBlocAnimal = makeABeautifulButton("", "/medias/objects/blocs/bloc-" + idAndColor.get(b.getIdColor()) + ".png");
					} else if(g[i][j] instanceof Wall) {
						// TODO : Image de MUR
						buttonBlocAnimal = new JButton("M");
						buttonBlocAnimal.setBackground(Color.DARK_GRAY);
						buttonBlocAnimal.setEnabled(false);
					} else {
						Animal a = (Animal) g[i][j];
						buttonBlocAnimal = makeABeautifulButton("", Animal.getImageAnimal().get(a.getI()));
						buttonBlocAnimal.setEnabled(false);
						buttonBlocAnimal.setDisabledIcon(new ImageIcon(getClass().getResource(Animal.getImageAnimal().get(a.getI()))));
					}
				} else {
					buttonBlocAnimal = new JButton("NULL");
					buttonBlocAnimal.setEnabled(false);
					buttonBlocAnimal.setVisible(false);
				}
				
				int x = i;
				int y = j;
				
				buttonBlocAnimal.addActionListener((event) -> {
					if(g[x][y] instanceof Bloc) {
						if(model.events.size() != 0) {
							String[] booster = {"bomb", "hammer", "rocket", "disco-ball"};
							for(String b : booster) {
								if(model.events.get(b) != null && model.events.get(b)) {
									level.removeBlocForBooster(x, y, new Booster(b));
									model.events.remove(b);
								} 
							} 
						} else {
							// On enlève les blocs et on soustrait le nombre de mouvements de 1
							level.removeBloc(x, y);
							level.getGoals().setNbMouv(level.getGoals().getNbMouv()-1);
						}
						// On fait descendre les blocs et on les décale
						level.fall();
						level.left();
						
											
						this.addPageLevels();
						interactionPlayerView.changePage("secondPageL" + level.getLvl());
					}
				});
				
				game.add(buttonBlocAnimal);
			}
		}
		return game;
	}
	
	/**
	 * Creation de la grille du haut du niveau
	 * @param Level level
	 * @return JPanel upPanel
	 */
	public JPanel makeGridUpLevel(Level level) {
		//Creation du JPanel
		JPanel upPanel = new JPanel(new BorderLayout());
		upPanel.setBounds(50, 0, 500, 120);
		upPanel.setBackground(new Color(0,0,0,0));
		
		// Creation du premier panneau
		JPanel goals = makePanelGoalsAndPoints(level, "OBJECTIFS", 0);
		upPanel.add(goals, BorderLayout.WEST);
		
		// Creation du 2e panneau
		JPanel m = makeCenterPanel(level);
		upPanel.add(m, BorderLayout.CENTER);
		
		// Creation du Panneau 3
		JPanel points = makePanelGoalsAndPoints(level, "POINTS", 20);
		upPanel.add(points, BorderLayout.EAST);
		
		return upPanel;
	}
	
	/**
	 * Creation du panneau des objectifs et points
	 * @param Level level
	 * @param String t
	 * @param int diff
	 * @return JPanel p
	 */
	public JPanel makePanelGoalsAndPoints(Level level, String t, int diff) {
		JPanel p = new JPanel(null);
		p.setPreferredSize(new Dimension(140, 100));
		p.setBackground(new Color(0,0,0,0));
				
		JLabel title = new JLabel(t, 0);
		title.setForeground(Color.white);
		title.setFont(new Font("SansSerif", Font.PLAIN, 10));
		title.setBackground(new Color(49, 109, 120));
		title.setBounds(25 + diff, 10, 70, 30);
		title.setOpaque(true);
		p.add(title);
		
		JPanel frame = new JPanel(null);
		frame.setBackground(Color.white);
		frame.setBounds(0 + diff, 20, 120, 80);
		frame.setBorder(BorderFactory.createLineBorder(new Color(49, 109, 120), 2));
		p.add(frame);
		
		// TODO : Remplir les objectifs et les points
		if(diff != 0) {
			JProgressBar bar = new JProgressBar(0, level.getGoals().getPoints());
			frame.add(bar, BorderLayout.CENTER);
			bar.setBounds(10, 10, 100, 60);
			bar.setStringPainted(true);
			bar.setFont(new Font("SansSerif", Font.PLAIN, 10));
			bar.setValue(level.getGoals().getPointsObtained());
			frame.add(bar);
		} else {
			JLabel image = this.makeABeautifulLabel("/medias/objects/animals/stefaon.png");
			image.setBounds(15, 15, 55, 55);
			JLabel txt = new JLabel(Integer.toString(level.getGoals().getAnimalToSave()-level.getGoals().getNbAnimalSave()));
			txt.setBounds(80, 15, 55,  55);
			txt.setFont(new Font("SansSerif", Font.PLAIN, 10));
			frame.add(image);
			frame.add(txt);
		}

		return p;
	}

	/**
	 * Creation du paneau au millieu de la page
	 * @param Level level
	 * @return JPanel m
	 */
	public JPanel makeCenterPanel(Level level) {
		JPanel m = new JPanel(new BorderLayout());
		m.setBackground(new Color(42, 109, 120));
		
		JLabel mTitle = new JLabel("MOUVEMENTS", 0);
		mTitle.setBorder(BorderFactory.createEmptyBorder(15, 0, 0, 0));
		mTitle.setFont(new Font("SansSerif", Font.PLAIN, 15));
		mTitle.setForeground(Color.white);
		m.add(mTitle, BorderLayout.NORTH);
		
		int movements = level.getGoals().getNbMouv();
		JLabel mTxt = new JLabel(Integer.toString(movements), 0);
		mTxt.setBorder(BorderFactory.createEmptyBorder(0, 0, 20, 0));
		mTxt.setFont(new Font("SansSerif", Font.PLAIN, 50));
		mTxt.setForeground(Color.white);
		m.add(mTxt, BorderLayout.CENTER);
		
		return m;
	}

}
