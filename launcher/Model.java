package launcher;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import controller.Player;
import dataBase.SerializeObject;
import objects.BoardGame;
import objects.Level;
/**
 * Cette classe permet stocker la vue
 */
public class Model {

	// VARIABLES
	private static View mainView = null;
	private static ViewTerminal viewTerminal = null;
	private static SerializeObject so = new SerializeObject();
		
	// VARIABLES PLATEAU (BOARDGAME)
	private final BoardGame boardGame;
	private final int NB_LVL_IN_A_PAGE = 20;
	private int nbPages = 1;
	private final int TotalNbPages = 1; // PHRASE QUI RISQUE DE CHANGER
	
	
	// VARIABLES LEVEL
	private Level partieEnCours;
	public Map<String, Boolean> events = new HashMap<String, Boolean>();
	private Map<Integer, String> idAndColor = new HashMap<Integer, String>();
	
	/**
	 * Retourne le niveau en cours
	 * @return le niveau en cours
	 */
	public Level getPartieEnCours() {
		return partieEnCours;
	}

	/**
	 * Change le niveau en cours
	 * @param partieEnCours nouveau niveau en cours  
	 */
	public void setPartieEnCours(Level partieEnCours) {
		this.partieEnCours = partieEnCours;
	}

	// CONSTRUCTEUR
	/**
	 * Construis un objet de type Model en lui donnant une vue
	 * @param v vue de l'interface graphique
	 */
	public Model (View v) throws ClassNotFoundException {
		this.mainView = v;
		this.boardGame = so.createBoardGameInitial(11); // PHRASE QUI RISQUE DE CHANGER
	}
	
	/**
	 * Construis un objet de type Model en lui donnant une Vue Terminal
	 * @param v vue du terminal
	 */
	public Model (ViewTerminal v) throws ClassNotFoundException {
		this.viewTerminal = v;
		this.boardGame = so.createBoardGameInitial(11); // PHRASE QUI RISQUE DE CHANGER
	}

	// ACCESSEURS (GETTEURS & SETTEURS)
	/**
	 * Retourne le plateau du jeu
	 * @return boardGame le plateau du jeu
	 */
	public BoardGame getBoardGame() { return boardGame; }
	/**
	 * Retourne le nombre de niveau sur une page
	 * @return NB_LVL_IN_A_PAGE le nombre de niveau sur une page
	 */
	public int getNB_LVL_IN_A_PAGE() { return NB_LVL_IN_A_PAGE; }
	/**
	 * Retourne le nombre de page
	 * @return nbPages le nombre de page
	 */
	public int getNbPages() { return nbPages; }
	/**
	 * Retourne le nombre total de page
	 * @return TotalNbPages le nombre total de page
	 */
	public int getTotalNbPages() { return TotalNbPages; }
	/**
	 * change le nombre page
	 * @param nbPages nombre de pages
	 */
	public void setNbPages(int nbPages) { this.nbPages = nbPages; }
	/**
	 * Retourne la map qui associe un entier a une couleur
	 * @return idAndColor la map qui associe un entier a une couleur
	 */
	public Map<Integer, String> getIdAndColor() { return idAndColor; }
	
	// METHODES
	/**
	 * 
	 * @param nbLvl nombre de niveau
	 * @return Level 
	 */
	public Level clone(int nbLvl) throws ClassNotFoundException {
		return so.deserializableLevels(nbLvl);
	}
	
	/**
	 * 
	 * @param p un joueur
	 */
	public void serializePlayer(Player p) {
		so.serializePlayer(p);
	}
	
	public Player deserializablePlayer(String name) {
		try {
			return so.deserializePlayer(name);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void addIdColor() {
		String[] color = {"B", "Y", "R", "G", "P"};
		int[] idDone = new int[color.length];
		Random rd = new Random();
		for(int k=0; k<color.length; k++) {
			int c = rd.nextInt(5) + 2;
			while(in(idDone, c)) {
				c = rd.nextInt(5) + 2;
			}	
			idDone[k] = c;
			idAndColor.put(k+2, color[c-2]);
		}
	}
	
	/**
	 * 
	 * @param s
	 * @param idColor 
	 * @return boolean
	 */
	public boolean in(int[] s, int idColor) {
		for(int i=0; i<s.length; i++) {
			if(s[i] == idColor) return true;
		}
		return false;
	}
	
	
}
