package objects;

import java.io.Serializable;
import java.util.Map;

/**
 * Cette classe représente un bloc de couleur dans un niveaux.
 * <p>Cette classe un attribut <em>color</em> de type String qui peut avoir 5 valeurs différentes : "B" (bleu), "G" (vert), "P" (violet), "R" (rouge), "Y" (jaune).</p>
 * Elle extend de la classe <b>GamePiece</b> parce qu'elle est une pièce insdispensable d'un niveau.
 * @see GamePiece
 */

public class Bloc extends GamePiece implements Viewable, Serializable {
	
	// VARIABLES
	private final int idColor;
	
	
	// CONSTRUCTEUR
	/**
	 * Construis un objet de type Bloc en lui donnant une couleur acceptée
	 * @param color couleur du bloc
	 */
	public Bloc(int idColor) {
		this.idColor = idColor;
	}

	
	// ACCESSEURS (GETTEURS & SETTEURS)
	/**
	 * Retourne la couleur du bloc
	 * @return la couleur du bloc
	 */
	public int getIdColor() { return idColor; }
	
	
	// METHODES	
	@Override
	public void displaysOnTheTerminal() {
		System.out.print(idColor);
	}
	
	public void displaysOnTheTerminal(Map<Integer, String> idAndColor) {
		System.out.print(idAndColor.get(idColor));
	}
	
}
