package objects;

/**
 * Viewable (Affichable) est une <b>interface qui possède une seule méthode</b>.<br>
 * Toutes les classes qui sont implémentés par cette interface sont considérés comme "Affichable".<br>
 * Ainsi tous les objets qui sont crées <b>peuvent s'afficher sur le terminal</b>.<br>
 */

public interface Viewable {
	
	/**
	 * Affiche sur le terminal l'object d'une classe qui est impléménté par Viewable
	 */
	public void displaysOnTheTerminal();

}
