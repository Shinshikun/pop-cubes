package objects;

import java.io.Serializable;
import java.util.Map;

/**
 * Cette classe représente un niveau <em>(dans le jeu de Pet Rescue Saga)</em>.
 * <p>Les évènements qui sont liés au jeu et toutes ses méthodes correspondantes sont ainsi décrites dans cette classe.
 * Le joueur, en utilisant les bons paramètres, peut supprimer des blocs.
 * Le jeu va automatiquement déplacer les blocs (soit en les descendant vers le sol (le bas du tableau) soit en les décalant vers la gauche).</p>
 */

public class Level implements Viewable, Serializable {
	
	// VARIABLES PROPRES AU NIVEAU
	private final int lvl;
	private boolean done = false;
	private boolean available = true; // A CHANGER A LA FIN
	private GamePiece[][] gamePiece;
	
	// OBJECTIFS ET COMPARATEUR
	private boolean[][] getAdja;
	private Goals goals;
	
	
	// CONSTRUCTEUR	
	/**
	 * Construis un objet de type Level
	 * en lui donnant le numéro du niveau, le nombre d'animaux à sauver, le nombre de points à obtenir, le nombre de mouvements et le tableau des blocs
	 * @param lvl le numéro du niveau
	 * @param animalToSave le nombre d'animaux à sauver
	 * @param points le nombre de points
	 * @param nbMouv le nombre de mouvements
	 * @param gamePieceInString le tableau des blocs
	 */
	public Level(int lvl, int animalToSave, int points, int nbMouv, int[][] gamePieceInInt) {
		this.lvl = lvl;
		this.goals = new Goals(animalToSave, points, nbMouv);		
		this.gamePiece = haveBlocTab(gamePieceInInt);
		this.getAdja = new boolean[gamePiece.length][gamePiece[0].length];
		
//		if(lvl == 1) available = true;
	}
	
	
	// ACCESSEURS (GETTEURS & SETTEURS)
	
	/**
	 * Retourne le numéro du level (niveau)
	 * @return le numéro du niveau
	 */
	public int getLvl() { return lvl; }
	
	/**
	 * Retourne true si le jouer a déjà terminé au moins une fois le (level) niveau
	 * @return si le joueur a déjà terminé au moins une fois le (level) niveau
	 */
	public boolean isDone() { return done; }
	
	/**
	 * Met à jour l'attribut done si le joueur a terminé le level (niveau) ou pas
	 * @param done 
	 */
	public void setDone(boolean done) { this.done = done; }
	
	/**
	 * Retourne true si le level (niveau) est disponible
	 * @return si le level (niveau) est disponible
	 */
	public boolean isAvailable() { return available; }
	
	/**
	 * Met à jour la disponibilité du level (niveau)
	 * @param available
	 */
	public void setAvailable(boolean available) { this.available = available; }
	
	/**
	 * Retourne les objectifs du level (niveau) que le joueur doit atteindre
	 * @return les objectifs du level (niveau)
	 */
	public Goals getGoals() { return goals; }
	
	/**
	 * Retourne le tableau de tableau des GamePiece (Pieces) qui sont contenues dans le niveau
	 * @return le tableau de tableau des GamePiece
	 * @see GamePiece
	 */
	public GamePiece[][] getGamePiece() { return gamePiece; }
	
	/**
	 * Met à jour le tableau de tableau des GamePiece (Pieces) qui sont contenues dans le niveau
	 * @param gamePiece
	 */
	public void setGamePiece(GamePiece[][] gamePiece) { this.gamePiece = gamePiece; }
	
	
	// METHODES
	
	/**
	 * Convertis un tableau de String[][] en GamePiece[][]
	 * @param gamePieceInString
	 * @return
	 */	
	public GamePiece[][] haveBlocTab(int[][] gamePieceInInt) {
		GamePiece[][] g = new GamePiece[gamePieceInInt.length][gamePieceInInt[0].length];
		for(int i=0; i<gamePieceInInt.length; i++) {
			for(int j=0; j<gamePieceInInt[i].length; j++) {
				if(gamePieceInInt[i][j] != 0) {
					if(gamePieceInInt[i][j] == 7) {
						g[i][j] = new Animal();
					} else if(gamePieceInInt[i][j] == 1) {
						g[i][j] = new Wall();
					} else {
						g[i][j] = new Bloc(gamePieceInInt[i][j]);
					}
				} else {
					g[i][j] = null;
				}
			}
		}
		return g;
	}
	
	/**
	 * 
	 * @param line
	 * @return
	 */
	public boolean lineIsEmpty(int line) {
		for(int i=0; i<gamePiece[line].length; i++) {
			if(gamePiece[line][i] instanceof Bloc) {
				return false;
			} else if(gamePiece[line][i] instanceof Animal) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * 
	 * @return
	 */
	public int haveTheFirstLineNoNull() {
		for(int i=0; i<gamePiece.length; i++) {
			if(!lineIsEmpty(i)) {
				return (i >= gamePiece.length-6 ? gamePiece.length-6 : i);
			}
		}
		return -1;
	}
	
	// METHODES EVENEMENT
	
	/**
	 * Enleve le bloc que le joueur veut enlever et vérifie si le mouvement est valide
	 * @param x coordonnées de la pièce
	 * @param y coordonnées de la pièce
	 */
	public void removeBloc(int x, int y) {
		if((gamePiece[x][y] instanceof Bloc && !(((Bloc) gamePiece[x][y]).getIdColor() == 1))) {
			int color = ((Bloc) gamePiece[x][y]).getIdColor();
			//TODO Conditions à faire
			int nbAdjacentsCases = getNbAdjacentsCases(x, y);
			goals.pointsObtained += nbAdjacentsCases*nbAdjacentsCases*10;
			this.removeAll(x, y, color);
			
		}
	}
	
	/**
	 * Enlève les blocs contigües (fonction auxiliaire de removeBloc(int x, int y))
	 * @param x coordonnées de la pièce
	 * @param y coordonnées de la pièce
	 * @param color couleur de la pièce
	 */
	public void removeAll(int x, int y, int color) {
		gamePiece[x][y] = null;
		if(isInsideTheTabVisible(x-1, y)) {
			if((gamePiece[x-1][y] != null) && (gamePiece[x-1][y] instanceof Bloc) && (((Bloc) gamePiece[x-1][y]).getIdColor() == color)) removeAll(x-1, y, color);
		}
		if(isInsideTheTabVisible(x+1, y)) {
			if((gamePiece[x+1][y] != null) && (gamePiece[x+1][y] instanceof Bloc) && (((Bloc) gamePiece[x+1][y]).getIdColor() == color)) removeAll(x+1, y, color);
		}
		if(isInsideTheTabVisible(x, y-1)) {
			if((gamePiece[x][y-1] != null) && (gamePiece[x][y-1] instanceof Bloc) && (((Bloc) gamePiece[x][y-1]).getIdColor() == color)) removeAll(x, y-1, color);
		}
		if(isInsideTheTabVisible(x, y+1)) {
			if((gamePiece[x][y+1] != null) && (gamePiece[x][y+1] instanceof Bloc) && (((Bloc) gamePiece[x][y+1]).getIdColor() == color)) removeAll(x, y+1, color);
		}
	}
	
	/**
	 * 
	 * @param x
	 * @param y
	 */
	public void removeBlocForBooster(int x, int y, Booster booster) {
		if(booster.getType().equals("hammer")) {
			gamePiece[x][y] = null;
		} else if(booster.getType().equals("bomb")) {
			if(gamePiece[x][y] instanceof Bloc) {
				gamePiece[x][y] = null;
				for(int i=x-1; i<=x+1; i++) {
					for(int j=y-1; j<=y+1; j++) {
						if(isInsideTheTabVisible(i, j)) {
							if(gamePiece[i][j] instanceof Bloc) gamePiece[i][j] = null;
							else if(gamePiece[i][j] instanceof Booster) removeBlocForBooster(i, j, (Booster) gamePiece[i][j]);
						}
					}
				}
			}
		} else if(booster.getType().equals("rocket")) {
			for(int i=0; i<gamePiece.length; i++) {
				if(isInsideTheTabVisible(i,y)) {
					if(gamePiece[i][y] instanceof Bloc) gamePiece[i][y] = null;
					else if(gamePiece[i][y] instanceof Booster) removeBlocForBooster(i, y, (Booster) gamePiece[i][y]);
				}
			}
		} else {
			int color = ((Bloc) gamePiece[x][y]).getIdColor();
			int max = haveTheFirstLineNoNull() + 6 >= gamePiece.length ? gamePiece.length : haveTheFirstLineNoNull() + 6;
			for(int i=this.haveTheFirstLineNoNull(); i<max; i++) {
				for(int j=0; j<gamePiece[i].length; j++) {
					if(gamePiece[i][j] != null && gamePiece[i][j] instanceof Bloc && ((Bloc) gamePiece[i][j]).getIdColor() == color) gamePiece[i][j] = null;
				}
			}
		}
		this.fall();
		this.left();
	}
	
	/**
	 * Descend les blocs vers le sol
	 */
	public void fall() {
		for(int j = 0; j < gamePiece[0].length; j++) {
			for(int i = gamePiece.length-1; i >= 0; i--) {
				gamePieceFall(i,j);
			}
		}
	}
	
	/**
	 * Peut savoir si le bloc aux coordonnées (i,j) peut descendre
	 * @param i (coordonnées du bloc)
	 * @param j (coordonnées du bloc)
	 * @return true si le bloc aux coordonnées (i,j) peut descendre
	 */
	private boolean canFall(int i, int j) {
		int max = haveTheFirstLineNoNull() + 6 >= gamePiece.length ? gamePiece.length : haveTheFirstLineNoNull() + 6;
		return (i < max-1 && gamePiece[i][j] != null && !(gamePiece[i][j] instanceof Wall) && gamePiece[i+1][j] == null);
	}

	/**
	 * Descend un bloc jusqu'à ce qu'il rencontre un obstacle
	 * @param i (coordonnées du bloc)
	 * @param j (coordonnées du bloc)
	 */
	private void gamePieceFall(int i, int j) {
		while(canFall(i, j)) {
			GamePiece tmp = gamePiece[i][j];
			gamePiece[i][j] = null;
			gamePiece[i+1][j] = tmp;
			i++;
		}
	}

	/**
	 * Décale les blocs vers la gauche s'il y a un obtacle sur leur chemin
	 * S'il y a rien à côté d'un mur (Mur), alors on déplace le bloc sur la gauche
	 * Si plusieurs blocs mur (Mur) sont côte à côte, alors on déplace le bloc en dessus (en diagonale)
	 * @return true si le décalage a été fait
	 */
	private boolean left1() {
		boolean b = false;
		int max = haveTheFirstLineNoNull() + 6 >= gamePiece.length ? gamePiece.length : haveTheFirstLineNoNull() + 6;
		for(int i=0; i<max; i++) {
			for(int j=0; j<gamePiece[i].length; j++) {
				if(gamePiece[i][j] instanceof Wall) {
					if(isInsideTheTabVisible(i, j-1) && gamePiece[i][j-1] == null || isInsideTheTabVisible(i, j-1) && (gamePiece[i][j-1] instanceof Wall && isInsideTheTabVisible(i-1, j-1) && gamePiece[i-1][j-1] == null)) {
						b = moveToLeft(i-1, j) || b;
					}
				}
			}
		}
		return b;
	}
	
	/**
	 * Décale les blocs de la dernière ligne.
	 * Si il y a une colonne vide, alors les blocs sont décalés vers la gauche.
	 * @return true si le décalage a été fait
	 */
	private boolean left2() {
		boolean b = false;
		int max = this.haveTheFirstLineNoNull()+6 > gamePiece.length ? gamePiece.length : this.haveTheFirstLineNoNull() + 6;
		for(int j = 1; j < gamePiece[0].length; j++) {
			if(gamePiece[max-1][j-1] == null && gamePiece[max-1][j] != null && !(gamePiece[max-1][j] instanceof Wall)) {
				b = moveToLeft(max-1,j) || b;
			}
		}
		return b;
	}

	/**
	 * Décale les blocs vers la gauche
	 * @param x (coordonnées du bloc)
	 * @param y (coordonnées du bloc)
	 * @return true si le décalage a été fait
	 */
	private boolean moveToLeft(int x, int y) {
		boolean b = false;
		for(int i = x; i >= 0; i--) {
			if(gamePiece[i][y] instanceof Wall || gamePiece[i][y] == null) break;
			if(gamePiece[i][y-1] == null) {
				GamePiece tmp = gamePiece[i][y];
				gamePiece[i][y] = null;
				gamePiece[i][y-1] = tmp;
				b = true;
			}		
		}
		fall();
		return b;
	}
		
	/**
	 * Décale les blocs vers la gauche tant qu'il y a un déplacement à faire
	 */
	public void left() {
		boolean l1 = true;
		boolean l2 = true;
		while(l1 || l2) {
			l1 = left1();
			l2 = left2();
		}
	}
	
	/**
	 * Vérifie si la pièce de coordonnées (x,y) est dans le tableau
	 * @param x (coordonnées du bloc)
	 * @param y (coordonnées du bloc)
	 * @return Retourne true si la pièce est dans le tableau
	 * @deprecated
	 */
	@Deprecated
	private boolean isInsideTheTab(int x, int y) {
		return (x>=0 && x<gamePiece.length && y>=0 && y<gamePiece[x].length);
	}
	
	/**
	 * Vérifie si les coordonnées (x,y) existent dans la partie visible du jeu
	 * @param x coordonnées de la pièce
	 * @param y coordonnées de la pièce
	 * @return si les coordonnées (x,y) existent dans la partie visible du jeu
	 */
	public boolean isInsideTheTabVisible(int x, int y) {
		int min = (haveTheFirstLineNoNull()+6 > gamePiece.length ? gamePiece.length : haveTheFirstLineNoNull()+6);
		return (x>=haveTheFirstLineNoNull() && x<min && y>=0 && y<gamePiece[x].length);
	}
	
	/**
	 * Retourne le nombre de cases adjacentes de la case donnée en paramêtres
	 * @param x coordonnées de la pièce
	 * @param y coordonnées de la pièce
	 * @return le nombre de cases adjacentes de la case donnée en paramêtre
	 */
	public int getNbAdjacentsCases(int x, int y) {
		this.setGetAdja(x, y);
		int cmp = 0;
		for(int i=0; i<getAdja.length; i++) {
			for(int j=0; j<getAdja[i].length; j++) {
				if(getAdja[i][j]) cmp++;
			}
		}
		
		for(int i=0; i<getAdja.length; i++) {
			for(int j=0; j<getAdja[i].length; j++) {
				getAdja[i][j] = false;
			}
		}
		return cmp;
	}
	
	/**
	 * Change les valeurs des cases adjacentes de la case mis en paramètre
	 * @param x 
	 * @param y
	 */
	public void setGetAdja(int x, int y) {
		int color = ((Bloc) gamePiece[x][y]).getIdColor();
		getAdja[x][y] = true;
		
		if(isInsideTheTabVisible(x-1, y)) {
			if((gamePiece[x-1][y] != null) && (gamePiece[x-1][y] instanceof Bloc) && (((Bloc) gamePiece[x-1][y]).getIdColor() == color) && (!getAdja[x-1][y])) setGetAdja(x-1,y);
		}
		if(isInsideTheTabVisible(x+1, y)) {
			if((gamePiece[x+1][y] != null) && (gamePiece[x+1][y] instanceof Bloc) && (((Bloc) gamePiece[x+1][y]).getIdColor() == color) && (!getAdja[x+1][y])) setGetAdja(x+1,y);
		}
		if(isInsideTheTabVisible(x, y-1)) {
			if((gamePiece[x][y-1] != null) && (gamePiece[x][y-1] instanceof Bloc) && (((Bloc) gamePiece[x][y-1]).getIdColor() == color) && (!getAdja[x][y-1])) setGetAdja(x,y-1);
		}
		if(isInsideTheTabVisible(x, y+1)) {
			if((gamePiece[x][y+1] != null) && (gamePiece[x][y+1] instanceof Bloc) && (((Bloc)gamePiece[x][y+1]).getIdColor() == color) && (!getAdja[x][y+1])) setGetAdja(x,y+1);
		}
		
		
	}
	
	@Override
	public void displaysOnTheTerminal() {
		System.out.println("################");
		System.out.println("#   Niveau " + this.lvl + "   #");
		System.out.println("################");
		System.out.println();
		System.out.println("Tu as sauvé " + goals.animalSaved + " / " + goals.animalToSave + " animaux.");
		System.out.println("Tu as obtenu " + goals.pointsObtained + " / " + goals.points + " points.");
		System.out.println();
		
		System.out.print("   ");
		for(int i=0; i<gamePiece[0].length; i++) {
			System.out.print((i+1) + " ");
		}
		System.out.println();
	}
	
	public void displaysOnTheTerminal(Map<Integer, String> idAndColor) {
		this.displaysOnTheTerminal();
		
		int max = haveTheFirstLineNoNull() + 6 >= gamePiece.length ? gamePiece.length : haveTheFirstLineNoNull() + 6;
		for(int i=haveTheFirstLineNoNull(); i<max; i++) {
			System.out.print((i+1) + "  ");
			for(int j=0; j<gamePiece[i].length; j++) {
				if(gamePiece[i][j] == null) System.out.print(" ");
				else {
					if(gamePiece[i][j] instanceof Bloc) gamePiece[i][j].displaysOnTheTerminal(idAndColor);
					else gamePiece[i][j].displaysOnTheTerminal();
				}
				System.out.print(" ");
			}
			System.out.println();
		}
		System.out.println();
		
		int nbLines = gamePiece.length - max;
		System.out.println("Il reste encore " + nbLines + " ligne" + (nbLines > 1 ? "s" : "") +" avant la fin du niveau.");
		System.out.println();
	}
	
	/**
	 * Goals (Objectifs) est une <b>classe interne</b> de la classe Level, elle décrit tous les objectifs que le joueur doit atteindre pour terminer le niveau.
	 * @see Level
	 */
	
	public class Goals implements Serializable {
		// VARIABLES
		private final int animalToSave;
		private final int points;
		private int animalSaved = 0;
		private int pointsObtained = 0;
		private int nbMouv;
		
		// CONTRUCTEUR
		/**
		 * Construis un object de type Goals en lui donnant un nombre d'animaux à sauver, et un nombre de points à atteindre
		 * @param animalToSave nombre d'animaux à sauver
		 * @param points nombre de points à obtenir
		 */
		public Goals(int animalToSave, int points, int nbMouv){
			this.animalToSave = animalToSave;
			this.points = points;
			this.nbMouv = nbMouv;
		}
		
		// ACCESSEURS (GETTEURS & SETTEURS)
		/**
		 * Retourne le nombre d'animaux à sauver
		 * @return le nombre d'animaux à sauver
		 */
		public int getAnimalToSave() { return animalToSave; }
		
		/**
		 * Retourne le nombre de points à obtenir
		 * @return retourne le nombre de points à obtenir
		 */
		public int getPoints() { return points; }
		
		/**
		 * Retourne le nombre d'animaux à sauver
		 * @return le nombre d'animaux à sauver
		 */
		public int getNbMouv() { return nbMouv; }
		
		/**
		 * Retourne le nombre de points à obtenir
		 * @return retourne le nombre de points à obtenir
		 */
		public void setNbMouv(int m) { nbMouv = m; }
		
		/**
		 * Retourne le nombre de points obtenus
		 * @return le nombre de points obtenus
		 */
		public int getPointsObtained() { return pointsObtained; }
		
		/**
		 * Retourne le nombre d'animaux sauvés
		 * @return le nombre d'animaux sauvés
		 */
		public int getNbAnimalSave() { return animalSaved; }
		
	}

}
