package objects;

import java.util.Map;

/**
 * Cette classe représente une pièce (que ce soit un bloc, un animal ou un booster).
 * Cette classe est abstraite parce qu'on ne peut pas créer un objet de ce type.
 */

public abstract class GamePiece implements Viewable {
	
	@Override
	public void displaysOnTheTerminal() {
		if(this instanceof Bloc) {
			Bloc b = (Bloc) this;
			b.displaysOnTheTerminal();
		} else if(this instanceof Wall) {
			Wall w = (Wall) this;
			w.displaysOnTheTerminal();
		} else {
			Animal a = (Animal) this;
			a.displaysOnTheTerminal();
		}
	}
	
	public void displaysOnTheTerminal(Map<Integer, String> idAndColor) {
		((Bloc) this).displaysOnTheTerminal(idAndColor);
	}
}
