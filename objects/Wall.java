package objects;

import java.io.Serializable;

public class Wall extends GamePiece implements Viewable, Serializable {
	
	@Override
	public void displaysOnTheTerminal() {
		System.out.print("#");
	}
	
}
