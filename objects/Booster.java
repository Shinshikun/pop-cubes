package objects;

public class Booster extends GamePiece implements Viewable {
	public String type;
	
	public Booster(String type) {
		this.type = type;
	}
	
	@Override
	public void displaysOnTheTerminal() {
		System.out.println(this.type);
	}
	
	
	// ACCESSEURS (GETTEURS & SETTEURS)
	public String getType() {
		return type;
	}
}
