package dataBase;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedList;

import controller.Player;
import objects.*;

public class SerializeObject {
	
	public void serializeLevel(Level l) {		
		try(FileOutputStream fos = new FileOutputStream("levels.txt");
			ObjectOutputStream oos = new ObjectOutputStream(fos)) {
			oos.writeObject(l);
			oos.close();
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public void serializeLevels() {
		KeepLevel keepLevel = new KeepLevel();
		Level one = keepLevel.getLevel1();
		Level two = keepLevel.getLevel2();
		Level three = keepLevel.getLevel3();
		Level four = keepLevel.getLevel4();
		Level five = keepLevel.getLevel5();
		Level six = keepLevel.getLevel6();
		Level seven = keepLevel.getLevel7();
		Level eight = keepLevel.getLevel8();
		Level nine = keepLevel.getLevel9();
		Level ten = keepLevel.getLevel10();
		Level eleven = keepLevel.getLevel11();
		Level test = keepLevel.getLeveltest();
		try(FileOutputStream fos = new FileOutputStream("levels.txt");
				ObjectOutputStream oos = new ObjectOutputStream(fos)) {
				oos.writeObject(one);
				oos.writeObject(two);
				oos.writeObject(three);
				oos.writeObject(four);
				oos.writeObject(five);
				oos.writeObject(six);
				oos.writeObject(seven);
				oos.writeObject(eight);
				oos.writeObject(nine);
				oos.writeObject(ten);
				oos.writeObject(eleven);
				oos.writeObject(test);
				oos.close();
			} catch(IOException e) {
				e.printStackTrace();
			}
	}
	
	public void serializePlayer(Player p) {
		try(FileOutputStream fos = new FileOutputStream(p.getName() + ".txt");
				ObjectOutputStream oos = new ObjectOutputStream(fos)) {
				oos.writeObject(p);
				oos.close();
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public Player deserializePlayer(String name) throws ClassNotFoundException {
		Player p = null;
		try(FileInputStream fis = new FileInputStream(name + ".txt");
			ObjectInputStream ois = new ObjectInputStream(fis)) {
			p = (Player) ois.readObject();
			ois.close();
		} catch(IOException e) {
			System.out.println("Vous n'avez pas de données en cours.");
		}
		return p;
	}
	
	public Level deserializableLevels(int lvl) throws ClassNotFoundException {
		Level l = null;
		try(FileInputStream fis = new FileInputStream("levels.txt");
			ObjectInputStream ois = new ObjectInputStream(fis)) {
			l = (Level) ois.readObject();
			while(l.getLvl() != lvl) {
				l = (Level) ois.readObject();
			}
			ois.close();
		} catch(IOException e) {
			e.printStackTrace();
		}
		return l;
	}
	
	public BoardGame createBoardGameInitial(int nbLvlFinal) throws ClassNotFoundException {
		LinkedList<Level> levelTab = new LinkedList<Level>();
		for(int i=1; i<=nbLvlFinal; i++) {
			Level l = deserializableLevels(i);
			levelTab.add(l);
		}
		
		Level[] levelTab2 = new Level[levelTab.size() + 1];
		int indice = 1;
		for(Level l : levelTab) {
			levelTab2[indice] = l;
			indice++;
		}
		
		BoardGame bg = new BoardGame(0, levelTab2.length-1, levelTab2);
		return bg;
	}

}
