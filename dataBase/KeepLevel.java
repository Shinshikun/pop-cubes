package dataBase;

import java.util.Random;

import objects.Level;

// FICHIER CREE POUR LA CREATION DE NIVEAUX, CE FICHIER NE SERA PAS FOURNI AU RAPPORT

// 0 = null
// 1 = Mur
// 2 -> 6 = Bloc
//7 = Animal

class KeepLevel {
	private Random rd = new Random();
	
	private int pointsNecessaire(int[][] s) {
		int bloc = s.length*s[0].length;
		for(int i=0; i<s.length; i++) {
			for(int j=0; j<s[i].length; j++) {
				if(s[i][j] == 1) {
					bloc--;
				}
			}
		}
		return bloc*30;
	}
	
	private boolean gameIsFull(int[][] s) {
		for(int i=0; i<s.length; i++) {
			for(int j=0; j<s[i].length; j++) {
				if(s[i][j] == 0) {
					return false;
				}
			}
		}
		return true;
	}
	
	private void setGameWithRandom(int[][] s, int nbColor) {
		int x = rd.nextInt(s.length);
		int y = rd.nextInt(s[x].length);
		int[] number = {2, 3, 4, 5, 6};
		while(!gameIsFull(s)) {
			int tmp = s[x][y];
			if(tmp == 0) {
				int c = rd.nextInt(nbColor);
				s[x][y] = number[c];
			} else {
				x = rd.nextInt(s.length);
				y = rd.nextInt(s[x].length);
			}
		}
	}
	
	public Level getLevel1() {
		int[][] s = {{7, 1, 1, 7, 1, 1, 7},
						{5, 5, 5, 3, 6, 6, 6},
						{5, 5, 5, 3, 6, 6, 6},
						{2, 2, 2, 3, 2, 2, 2},
						{2, 2, 2, 3, 2, 2, 2},
						{2, 2, 2, 3, 2, 2, 2}};
		Level lvl = new Level(1, 3, pointsNecessaire(s), 5, s);
		return lvl;
	}
	
	public Level getLevel2() {
		int[][] s = {{1, 7, 1, 1, 1, 7, 1},
						{2, 2, 4, 4, 4, 2, 2},
						{2, 2, 3, 3, 3, 2, 2},
						{4, 4, 3, 3, 3, 5, 5},
						{4, 4, 3, 3, 3, 4, 4},
						{2, 2, 6, 6, 6, 4, 4}};
		Level lvl = new Level(2, 2, pointsNecessaire(s), 10, s);
		return lvl;
	}
	
	public Level getLevel3() {
		int[][] s = {{1, 2, 2, 3, 2, 2, 1},
						{7, 2, 2, 3, 2, 2, 7},
						{2, 4, 4, 3, 4, 4, 2},
						{2, 4, 4, 3, 4, 4, 2},
						{4, 2, 2, 3, 2, 2, 4},
						{4, 2, 2, 3, 2, 2, 4}};
		Level lvl = new Level(3, 2, pointsNecessaire(s), 10, s);
		return lvl;
	}
	
	public Level getLevel4() {
		int[][] s = {{1, 1, 7, 0, 7, 0, 7},
						{1, 1, 5, 3, 4, 3, 4},
						{1, 1, 5, 3, 4, 3, 4},
						{1, 3, 4, 5, 3, 4, 5},
						{1, 3, 4, 5, 3, 4, 5},
						{3, 4, 5, 3, 4, 5, 3},
						{3, 4, 5, 3, 4, 5, 3}};
		Level lvl = new Level(4, 3, pointsNecessaire(s), 10, s);
		return lvl;
	}
	
	public Level getLevel5() {
		int[][] s = {{1, 1, 1, 1, 7, 7, 7},
						{1, 1, 1, 2, 4, 5, 6},
						{1, 1, 3, 2, 4, 5, 6},
						{1, 5, 3, 2, 4, 5, 1},
						{6, 5, 3, 2, 4, 1, 1},
						{6, 5, 3, 2, 1, 1, 1}};
		Level lvl = new Level(5, 3, pointsNecessaire(s), 10, s);
		return lvl;
	}
	
	public Level getLevel6() {
		// INITIATION A LA FUSEE
		int[][] s = {{1, 7, 7, 1, 1, 1, 1},
						{1, 5, 6, 1, 1, 1, 1},
						{6, 2, 6, 6, 6, 6, 6},
						{6, 6, 6, 6, 6, 6, 6},
						{6, 6, 6, 6, 6, 6, 6},
						{6, 6, 6, 6, 6, 6, 6}};
		Level lvl = new Level(6, 2, pointsNecessaire(s), 1, s);
		return lvl;
	}
	
	public Level getLevel7() {
		int[][] s = {{1, 1, 1, 7, 1, 1, 1},
						{1, 1, 0, 0, 0, 1, 1},
						{1, 0, 0, 0, 0, 0, 1},
						{0, 0, 0, 0, 0, 0, 0},
						{0, 0, 0, 0, 0, 0, 0},
						{0, 0, 0, 0, 0, 0, 0}};
		setGameWithRandom(s,4);
		Level lvl = new Level(7, 1, pointsNecessaire(s), 20, s);
		return lvl;
	}
	
	public Level getLevel8() {
		int[][] s = {{1, 1, 7, 1, 7, 1, 1},
						{1, 7, 0, 0, 0, 7, 1},
						{0, 0, 0, 0, 0, 0, 0},
						{0, 0, 1, 0, 1, 0, 0},
						{0, 0, 0, 0, 0, 0, 0},
						{0, 1, 0, 1, 0, 1, 0},
						{0, 1, 0, 1, 0, 1, 0},
						{0, 1, 0, 1, 0, 1, 0},
						{0, 0, 0, 0, 0, 0, 0},
						{0, 0, 0, 0, 0, 0, 0},
						{0, 0, 0, 0, 0, 0, 0},
						{0, 0, 0, 0, 0, 0, 0}};
		setGameWithRandom(s,2);
		Level lvl = new Level(8, 4, pointsNecessaire(s), 25, s);
		return lvl;
	}
	
	public Level getLevel9() {
		int[][] s = {{1, 1, 1, 1, 1, 1, 7},
						{1, 1, 1, 1, 1, 7, 7},
						{1, 1, 1, 1, 0, 0, 0},
						{1, 1, 1, 0, 0, 0, 0},
						{1, 1, 0, 0, 0, 0, 0},
						{1, 0, 0, 0, 0, 0, 1},
						{0, 0, 0, 0, 0, 1, 1},
						{0, 0, 0, 0, 1, 1, 1},
						{0, 0, 0, 1, 1, 1, 1},
						{0, 0, 0, 0, 1, 1, 1},
						{0, 0, 0, 0, 0, 1, 1},
						{0, 0, 0, 0, 0, 0, 0},
						{0, 0, 1, 0, 1, 0, 0},
						{0, 0, 1, 0, 1, 0, 0},
						{0, 0, 0, 0, 0, 0, 0},
						{0, 0, 0, 0, 0, 0, 0},
						{0, 0, 0, 0, 0, 0, 0},
						{0, 0, 0, 0, 0, 0, 0}};
		setGameWithRandom(s, 3);
		Level lvl = new Level(9, 3, pointsNecessaire(s), 30, s);
		return lvl;
	}
	
	public Level getLevel10() {
		int[][] s = {{0, 0, 1, 1, 1, 0, 7},
					{0, 7, 0, 7, 0, 7, 0},
					{0, 1, 0, 1, 0, 1, 0},
					{0, 1, 0, 1, 0, 1, 7},
					{0, 0, 0, 0, 0, 0, 0},
					{0, 1, 0, 1, 0, 1, 0},
					{0, 1, 0, 1, 0, 1, 0},
					{0, 0, 0, 0, 0, 0, 0}};
		setGameWithRandom(s, 2);
		Level lvl = new Level(10, 5, pointsNecessaire(s), 15, s);
		return lvl;
	}
	
	public Level getLevel11() {
		int[][] s = {{2, 1, 1, 1, 1, 4, 1},
					{2, 1, 1, 1, 1, 4, 1},
					{2, 4, 4, 3, 4, 4, 2},
					{2, 4, 4, 3, 4, 4, 2},
					{4, 2, 2, 3, 2, 2, 4},
					{4, 2, 2, 3, 2, 2, 4},
					{1, 4, 4, 3, 4, 4, 1},
					{7, 4, 4, 3, 4, 4, 7},
					{2, 4, 4, 3, 4, 4, 2},
					{2, 4, 4, 3, 4, 4, 2},
					{4, 2, 2, 3, 2, 2, 4},
					{4, 2, 2, 3, 2, 2, 4}};
		Level lvl = new Level(11, 2, pointsNecessaire(s), 10, s);
		return lvl;
	}

	public Level getLeveltest() {
		int[][] s = {{2, 1, 1, 1, 1, 4, 1},
						{2, 1, 1, 1, 1, 4, 1},
						{2, 4, 4, 3, 4, 4, 2},
						{2, 4, 4, 3, 4, 4, 2},
						{4, 2, 2, 3, 2, 2, 4},
						{4, 2, 2, 3, 2, 2, 4},
						{1, 4, 4, 3, 4, 4, 1},
						{7, 4, 4, 3, 4, 4, 7},
						{2, 4, 4, 3, 4, 4, 2},
						{2, 4, 4, 3, 4, 4, 2},
						{4, 2, 2, 3, 2, 2, 4},
						{4, 2, 2, 3, 2, 2, 4}};
		Level lvl = new Level(11, 2, pointsNecessaire(s), 10, s);
		return lvl;
	}
	
	public Level getLevelTemplate() {
		int[][] s = {{0, 0, 0, 0, 0, 0, 0},
						{0, 0, 0, 0, 0, 0, 0},
						{0, 0, 0, 0, 0, 0, 0},
						{0, 0, 0, 0, 0, 0, 0},
						{0, 0, 0, 0, 0, 0, 0},
						{0, 0, 0, 0, 0, 0, 0}};
		Level lvl = new Level(7, 7, pointsNecessaire(s), 7, s);
		return lvl;
	}

}
